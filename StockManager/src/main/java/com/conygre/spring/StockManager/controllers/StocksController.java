package com.conygre.spring.StockManager.controllers;

import java.util.Collection;

import com.conygre.spring.StockManager.entities.Stocks;
import com.conygre.spring.StockManager.service.StocksService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stocks")
public class StocksController {

    @Autowired
    private StocksService service;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Stocks> getStocks(){
        return service.getStocks();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addStocks(@RequestBody Stocks stocks){
        service.addStocks(stocks);
    }
    
}