package com.conygre.spring.StockManager.repo;

import com.conygre.spring.StockManager.entities.Stocks;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StocksRepository extends MongoRepository<Stocks,ObjectId> 

{

    
}