package com.conygre.spring.StockManager.service;

import java.util.Collection;

import com.conygre.spring.StockManager.entities.Stocks;


public interface StocksService {

    Collection<Stocks> getStocks();
    void addStocks(Stocks stocks);
    
}