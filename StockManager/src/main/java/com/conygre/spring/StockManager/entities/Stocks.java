package com.conygre.spring.StockManager.entities;

import java.sql.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stocks {
    @Id
    private ObjectId id;
    private String DateCreated;
    private int StockQuantity;
    private double RequestedPrice;
    enum TradeStatus{VREATED,PENDING,CANCELLED,REJECTED,FILLED,PARTIALLY_FIILED,ERROR}

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getDateCreated() {
        return  DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public int getStockQuantity() {
        return StockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        StockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return RequestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        RequestedPrice = requestedPrice;
    };

    
}