package com.conygre.spring.StockManager.service;

import java.util.Collection;

import com.conygre.spring.StockManager.entities.Stocks;
import com.conygre.spring.StockManager.entities.*;
import com.conygre.spring.StockManager.repo.StocksRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StocksServiceImpl implements StocksService {

    @Autowired
    private StocksRepository repo;

    @Override
    public Collection<Stocks> getStocks() {
       
        return repo.findAll();
    }

    @Override
    public void addStocks(Stocks stocks) {
       repo.insert(stocks);
    }


    
}